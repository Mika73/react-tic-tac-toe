# Tic Tac Toe
Tic Tac Toe game created with react.
https://timely-sable-1c5af8.netlify.app/

# Requirements

- Node 16+

## Quick start

```
npm install && npm start
```

## Test
We used [jest-dom](https://github.com/testing-library/jest-dom) and [wait-for-expect](https://github.com/TheBrainFamily/wait-for-expect) in our tests.

```
npm test
```

import React, { useState, useEffect } from "react";
import Board from "./Board";
import trophy from "../img/trophy.png";

const Game = () => {

  if (!localStorage.getItem("round")) {
    localStorage.setItem("round", 1);
  }

  const [history, setHistory] = useState([Array(9).fill(null)]);
  const [winLossRecords, setWinLossRecord] = useState([]);
  const [round, setRound] = useState(localStorage.getItem("round"));
  const [stepNumber, setStepNumber] = useState(0);
  const winner = calculateWinner(history[stepNumber]);
  const players = {
    CPU: {
      SYM: "O",
      NAME: "CPU",
    },
    HUMAN: {
      SYM: "X",
      NAME: "You",
    },
  };

  useEffect(() => {
    let i = 1;
    let roundHistory = [];
    while (localStorage.getItem(`Round${i}`)) {
      roundHistory = [...roundHistory, localStorage.getItem(`Round${i}`)];
      i++;
    }
    setWinLossRecord(roundHistory);
    return;
  }, []);

  const handleClick = (i) => {
    const historyPoint = history.slice(0, stepNumber + 1);
    const current = historyPoint[stepNumber];
    const squares = [...current];
    // return if won or occupied
    if (winner || squares[i]) return;
    // select square
    squares[i] = players.HUMAN.SYM;
    setHistory([...historyPoint, squares]);
    setStepNumber(historyPoint.length);
    cpuPlay(squares);
  };

  const cpuPlay = (squares) => {
    let available = [];
    for (let i = 0; i < squares.length; i++) {
      if (squares[i] === null) {
        available.push(i);
      }
    }
    let rand = Math.floor(Math.random() * available.length);
    var move = available[rand];
    squares[move] = players.CPU.SYM;
    const historyPoint = history.slice(0, stepNumber + 1);
    setHistory([...historyPoint, squares]);
    setStepNumber(historyPoint.length);
  };

  const jumpTo = (step) => {
    setStepNumber(step);
  };

  const renderMoves = () =>
    history.map((_step, move) => {
      const destination = move ? `Go to move #${move}` : "Go to game start";
      return (
        <li key={move}>
          <button data-testid="move" onClick={() => jumpTo(move)}>
            {destination}
          </button>
        </li>
      );
    });

  const restart = (winner) => {
    if (winner) {
      setWinLossRecord([
        ...winLossRecords,
        winner === players.HUMAN.SYM ? players.HUMAN.NAME : players.CPU.NAME,
      ]);

      localStorage.setItem(
        `Round${round}`,
        winner === players.HUMAN.SYM ? players.HUMAN.NAME : players.CPU.NAME
      );

      localStorage.setItem(
        "round",
        parseInt(localStorage.getItem("round")) + 1
      );
      setRound(localStorage.getItem("round"));
    }

    setHistory([Array(9).fill(null)]);
    setStepNumber(0);
  };

  const renderRoundHistory = () =>
    winLossRecords.map((record, i) => {
      return (
        <li className="round-history" key={i}>
          Round{i + 1} : {record} won
        </li>
      );
    });

  const renderWinner = () => {
    if (winner) {
      return (
        <div>
          <img className="trophy" src={trophy} alt="Trophy"></img>
          <h2 data-testid="winner">
            {winner === players.HUMAN.SYM
              ? players.HUMAN.NAME
              : players.CPU.NAME}{" "}
            won !
          </h2>
        </div>
      );
    }
  };

  function calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (
        squares[a] &&
        squares[a] === squares[b] &&
        squares[a] === squares[c]
      ) {
        return squares[a];
      }
    }
    return null;
  }

  return (
    <>
      <header>
        <h1>Tic Tac Toe</h1>
        {renderWinner()}
        <button data-testid="btn-restart" onClick={() => restart(winner)}>
          From the beginning
        </button>
      </header>
      <div className="game-wrapper">
        <div className="game-result">
          <h3>History</h3>
          {renderMoves()}
        </div>
        <Board squares={history[stepNumber]} onClick={handleClick} />
        <div className="game-result">
          <h3>Previous games</h3>
          {renderRoundHistory()}
        </div>
      </div>
    </>
  );
};

export default Game;

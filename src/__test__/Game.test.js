import { render, screen, fireEvent } from "@testing-library/react";
import waitForExpect from "wait-for-expect";
import Game from "../components/Game";

describe("Test Game", () => {
  test("render form with 11 button", async () => {
    render(<Game />);
    const buttonList = await screen.findAllByRole("button");
    expect(buttonList).toHaveLength(11);
  });

  test("display squares with initial state", async () => {
    render(<Game />);

    // screen.debug()

    const squares = screen.queryAllByTestId("square");
    expect(squares.length).toBe(9);

    expect(squares[0].textContent).toBe("");
    expect(squares[1].textContent).toBe("");
    expect(squares[2].textContent).toBe("");
    expect(squares[3].textContent).toBe("");
    expect(squares[4].textContent).toBe("");
    expect(squares[5].textContent).toBe("");
    expect(squares[6].textContent).toBe("");
    expect(squares[7].textContent).toBe("");
    expect(squares[8].textContent).toBe("");

    expect(screen.queryAllByTestId("move").length).toBe(1);
    expect(screen.queryAllByTestId("move")[0].textContent).toBe(
      "Go to game start"
    );
  });

  // test("X as winner", async () => {
  //   render(<Game />);

  //   const squares = screen.queryAllByTestId("square");

  //   fireEvent.click(squares[0]);
  //   fireEvent.click(squares[1]);
  //   fireEvent.click(squares[2]);

  //   await waitForExpect(
  //     () => {
  //       expect(screen.getByTestId("winner").textContent).toBe("You won !");
  //     },
  //     2000,
  //     50
  //   );
  // });

  test("display of history of moves", async () => {
    render(<Game />);

    const squares = screen.queryAllByTestId("square");

    expect(screen.queryAllByTestId("move").length).toBe(1);
    expect(screen.queryAllByTestId("move")[0].textContent).toBe(
      "Go to game start"
    );
    fireEvent.click(squares[0]);
    await waitForExpect(() => {
      expect(screen.queryAllByTestId("move").length).toBe(2);
    });
    expect(screen.queryAllByTestId("move")[1].textContent).toBe(
      "Go to move #1"
    );
    fireEvent.click(squares[4]);
    await waitForExpect(() => {
      expect(screen.queryAllByTestId("move").length).toBe(3);
    });
    expect(screen.queryAllByTestId("move")[2].textContent).toBe(
      "Go to move #2"
    );
  });
});
